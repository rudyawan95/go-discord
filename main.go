package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"
	"strings"

	"oddlyegg/go-discord/utils"
	"oddlyegg/go-discord/commands"

	"github.com/bwmarrin/discordgo"
)

var (
	TOKEN, PREFIX string
	CmdHandler *utils.CommandHandler
)

func init() {
	TOKEN = "NTEzOTY5MDk1NDQ4ODU0NTM3.XdS3kQ.Bax9Lpiml1qpZVFkORTP4FoUTg4"
	PREFIX = "!"
}

func main() {
	CmdHandler = utils.NewCommandHandler()
	registerCommands()

	dg, err := discordgo.New("Bot " + TOKEN)

	if err != nil {
		log.Print("Error creating Discord session: ,", err)
		return
	}

	dg.AddHandler(messageHandler)

	err = dg.Open()
	if err != nil {
		log.Print("Error opening connection: ,", err)
		return
	}

	log.Printf("Bot %s is now running.", dg.State.User.Username)

	for _, guild := range dg.State.Guilds {
		log.Printf("Bot is listening to server '%s'", guild.ID)
	}

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill) 
	<-sc

	dg.Close()
}

func messageHandler(s *discordgo.Session, m *discordgo.MessageCreate) {
	user := m.Author
	if user.ID == s.State.User.ID {
		return
	}
	content := m.Content
	if len(content) <= len (PREFIX) {
		return
	}
	if content[:len(PREFIX)] != PREFIX {
		return
	}

	content = content[len(PREFIX):]
	args := strings.Fields(content)

	channel, err := s.State.Channel(m.ChannelID)
	if err != nil {
		log.Print("Error getting session's channel,", err)
		return
	}

	command, found := CmdHandler.Get(args[0])
	if !found {
		return
	}
	context := utils.NewContext(s, channel, args, user)
	cmd  := *command
	cmd(*context)
}

func registerCommands(){
	CmdHandler.Register("ping", commands.PingCommand, "Ping Command")
	CmdHandler.Register("hello", commands.HelloCommand, "Hello Command")
	CmdHandler.Register("join", commands.JoinCommand, "Join Command")
	CmdHandler.Register("leave", commands.LeaveCommand, "Leave Command")
}