package utils

import (
	"net/http"
	"fmt"
	"encoding/binary"
	"io"
	"log"
)

var buffer = make([][]byte, 0)

func PlayAudio(url string) error {
	resp, err := http.Get(url)
	if err != nil {
		err := fmt.Errorf("Failed to download youtube audio: ", err)
		return err
	}
	log.Print("download successful")

	var opuslen int16

	for {
		log.Print("read audio opuslen")
		err = binary.Read(resp.Body, binary.LittleEndian, &opuslen)
		
		if err == io.EOF || err == io.ErrUnexpectedEOF {
			err := resp.Body.Close()
			if err != nil {
				return err
			}
			return nil
		}

		if err != nil {
			fmt.Println("Error reading from dca file :", err)
			return err
		}

		log.Print("read audio inbuf")
		InBuf := make([]byte, opuslen)
		err = binary.Read(resp.Body, binary.LittleEndian, &InBuf)

		if err != nil {
			fmt.Println("Error reading from dca file :", err)
			return err
		}

		buffer = append(buffer, InBuf)
	}
}