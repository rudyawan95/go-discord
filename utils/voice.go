package utils

import (
	"github.com/bwmarrin/discordgo"
)

type Voice struct {
	VoiceConnection		*discordgo.VoiceConnection
	Speaking			bool
	Idle				bool
}

func (v Voice) Disconnect() {
	v.VoiceConnection.Disconnect()
}

func NewVoiceConnection(context Context) *Voice {
	v := new(Voice)
	v.VoiceConnection, _ = context.Session.ChannelVoiceJoin(context.Guild.ID, context.VoiceChannel.ID, true, false)
	v.Speaking = false
	v.Idle = true
	return v
}