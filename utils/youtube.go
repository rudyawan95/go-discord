package utils

import (
	"net/url"
	"fmt"
	"log"
	"io/ioutil"
	"net/http"
	"strings"
	"sort"
	"strconv"
	
	"github.com/buger/jsonparser"
)

type stream map[string]string
type youtube struct {
	streamList []stream
	videoID    string
	videoInfo  string
}

var (
	GOOGLEKEY string
)

func init() {
	GOOGLEKEY = "AIzaSyAt2p7Da_ynBmu4K0bNoU7XeVdXjvGsIQw"
}

func searchYoutube(text string) ([]string, error) {
	var videoIDs []string
	url := fmt.Sprintf("https://www.googleapis.com/youtube/v3/search?part=snippet&q=%s&maxResults=10&type=video&key=%s", url.QueryEscape(text), GOOGLEKEY)
	resp, err := http.Get(url)
	if err != nil {
		err = fmt.Errorf("Unable to connect to google youtube API: ", err)
		return videoIDs, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		err = fmt.Errorf("Failed on reading youtube search result: ", err)
		return videoIDs, err 
	}
	_, err = jsonparser.ArrayEach(body, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
		id, err := jsonparser.GetString(value, "id", "videoId")
		videoIDs = append(videoIDs, id)
	}, "items")
	if err != nil {
		err = fmt.Errorf("Could not find items in youtube search result")
		return videoIDs, err 
	}
	if len(videoIDs) == 0 {
		err = fmt.Errorf("No result found for '%s'", text)
	}
	return videoIDs, err
}

func (y *youtube) getVideoInfo() error {
	url := "http://youtube.com/get_video_info?video_id=" + y.videoID

	resp, err := http.Get(url)
	if err != nil {
		return fmt.Errorf("Unable to connect to youtube server: ", err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		err = fmt.Errorf("Failed on reading youtube server's answer: ", err)
		return err
	}
	y.videoInfo = string(body)
	return nil
}

func (y *youtube) parseVideoInfo() error {
	answer, err := url.ParseQuery(y.videoInfo)
	if err != nil {
		err = fmt.Errorf("Failed on parsing youtube server's answer: ", err)
		return err
	}
	status, ok := answer["status"]
	if !ok {
		err = fmt.Errorf("No response status found in youtube server's answer")
		return err
	}
	if status[0] == "fail" {
		reason, ok := answer["reason"]
		if ok {
			err = fmt.Errorf("'fail' response status found in the server's answer, reason: '%s'", reason[0])
		} else {
			err = fmt.Errorf("'fail' response status found in the server's answer, no reason given")
		}
		return err
	}
	if status[0] != "ok" {
		err = fmt.Errorf("Non-success response status found in the server's answer (status: '%s')", status)
		return err
	}

	streamMap, ok := answer["adaptive_fmts"]
	if !ok {
		err = fmt.Errorf("No stream map found in the server's answer")
		return err
	}
	for _, streamRaw := range strings.Split(streamMap[0], ",") {
		streamQry, err := url.ParseQuery(streamRaw)
		if err != nil {
			err = fmt.Errorf("Failed on parsing stream map", err)
			return err
		}
		if len(streamQry["audio_channels"]) > 0 {
			stream := stream{
				"itag" : streamQry["itag"][0],
				"type" : streamQry["type"][0],
				"bitrate" :	streamQry["bitrate"][0],
				"audio_channels" : streamQry["audio_channels"][0],
				"url" : streamQry["url"][0],
			}
			y.streamList = append(y.streamList, stream)
		}		
	}
	sort.Slice(y.streamList, func(i, j int) bool { 
		i_ , _ := strconv.Atoi(y.streamList[i]["bitrate"]) 
		j_ , _ := strconv.Atoi(y.streamList[j]["bitrate"])
		return  i_ > j_
	})
	return nil
}

func Youtube(text string) {
	y := new(youtube)
	videoIDs, err := searchYoutube(text)
	if err != nil {
		log.Print(err)
		return
	}

	y.videoID = videoIDs[0]
	log.Print(y.videoID)

	err = y.getVideoInfo()
	if err != nil {
		log.Print(err)
		return
	}
	err = y.parseVideoInfo()
	if err != nil {
		log.Print(err)
		return
	}

	for i := 0; i < len(y.streamList); i++ {
		log.Print(y.streamList[i]["bitrate"])
	}

	log.Print(y.streamList[0]["url"])

	// err = PlayAudio(y.streamList[0]["url"])
	// if err == nil {
	// 	log.Print("Audio Played")
	// }
}