package utils

import (
	"github.com/bwmarrin/discordgo"
	"log"
)


type Context struct {
	Session			*discordgo.Session
	TextChannel		*discordgo.Channel
	User			*discordgo.User
	Args			[]string
	Response		string

	Guild			*discordgo.Guild
	VoiceChannel	*discordgo.Channel
	Voice 			*Voice
}


func (c *Context) Reply() {
	log.Printf("Sending message to channel '%s' with content '%s'", c.TextChannel.ID, c.Response)
	c.Session.ChannelMessageSend(c.TextChannel.ID, c.Response)
}

func NewContext(session *discordgo.Session, textChannel *discordgo.Channel, args []string, user *discordgo.User) *Context {
	log.Printf("Creating context triggered by user '%s' with command '%s'", user.ID, args[0])
	c := new(Context)
	c.Session = session
	c.TextChannel = textChannel
	c.Args = args
	c.User = user
	return c
}

func (c Context) GetGuild() (*discordgo.Guild, error) {
	guild, err := c.Session.State.Guild(c.TextChannel.GuildID)
	return guild, err
}

func (c Context) GetVoiceChannel() (*discordgo.Channel, error) {
	for _, vs := range c.Guild.VoiceStates {
		if vs.UserID == c.User.ID {
			vc, err := c.Session.State.Channel(vs.ChannelID)
			if err != nil {
				return vc, err
			}
			return vc, err
		}
	}
	return nil, nil 
}
