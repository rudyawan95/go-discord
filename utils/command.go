package utils

type (
	Command func(Context)

    CommandStruct struct {
		command Command
		help string
	}

	CommandMap map[string]CommandStruct

	CommandHandler struct {
		commands CommandMap
	}
)

func NewCommandHandler() *CommandHandler {
	return &CommandHandler{make(CommandMap)}
}

func (handler CommandHandler) Get(name string) (*Command, bool) {
	cmd, found := handler.commands[name]
	return &cmd.command, found
}

func (handler CommandHandler) Register(name string, command Command, helpmsg string) {
	cmdstruct := CommandStruct{command: command, help: helpmsg}
	handler.commands[name] = cmdstruct
	if len(name) > 1 {
		handler.commands[name[:1]] = cmdstruct
	}
}