package commands

import (
	"oddlyegg/go-discord/utils"
	"fmt"
	"log"
)

func JoinCommand(c utils.Context) {
	var err error
	c.Guild, err = c.GetGuild()
	if err != nil {
		log.Print(err)
		return 
	}

	c.VoiceChannel, err = c.GetVoiceChannel()
	if err != nil {
		log.Print(err)
		return 
	}

	c.Voice = utils.NewVoiceConnection(c)

	c.Response = fmt.Sprintf("Joining voice channel %s", c.VoiceChannel.Mention())
	c.Reply()
	// c.JoinVoiceChannel()
}