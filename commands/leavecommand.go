package commands

import (
	"oddlyegg/go-discord/utils"
	"fmt"
)

func LeaveCommand(c utils.Context) {
	c.Response = fmt.Sprintf("Leaving voice channel")
	c.Voice.Disconnect()
}