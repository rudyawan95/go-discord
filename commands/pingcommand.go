package commands

import (
	"oddlyegg/go-discord/utils"
)

func PingCommand(c utils.Context) {
	c.Response = "pong"
	c.Reply()
}