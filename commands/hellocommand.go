package commands

import (
	"oddlyegg/go-discord/utils"

	"fmt"
)

func HelloCommand(c utils.Context) {
	c.Response = fmt.Sprintf("hi, %s", c.User.Mention())
	c.Reply()
}